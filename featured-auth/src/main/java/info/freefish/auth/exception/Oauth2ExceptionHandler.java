package info.freefish.auth.exception;

import info.freefish.auth.api.CommonResult;
import info.freefish.common.exception.BizCodeEnume;
import info.freefish.common.utils.R;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 全局处理Oauth2抛出的异常
 */
@ControllerAdvice
public class Oauth2ExceptionHandler {
    @ResponseBody
    @ExceptionHandler(value = OAuth2Exception.class)
    public R handleOauth2(OAuth2Exception e) {
        return R.error(BizCodeEnume.AUTH_SERVER_FAILED.getCode(), BizCodeEnume.AUTH_SERVER_FAILED.getMsg());
    }

    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public R handleException(Exception e) {
        return R.error();
    }

}
