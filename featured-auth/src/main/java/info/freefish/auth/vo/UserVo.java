package info.freefish.auth.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import java.io.Serializable;
import java.util.List;

/**
 * 用户信息表
 *
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-05-24 15:20:25
 */
@Data
public class UserVo implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 用户名
     */
    private String username;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 性别
     */
    private Integer gender;
    /**
     * 密码
     */
    private String password;
    /**
     * 部门ID
     */
    private Integer deptId;

    /**
     * 用户头像
     */
    private String avatar;
    /**
     * 联系方式
     */
    private String mobile;

    /**
     * 用户邮箱
     */
    private String email;

    /**
     * role id 集合
     */
    private List<Long> roleIds;

}
