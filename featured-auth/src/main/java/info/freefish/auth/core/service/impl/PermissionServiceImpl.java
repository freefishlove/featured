package info.freefish.auth.core.service.impl;

import info.freefish.auth.core.dto.PermissionIndexDto;
import info.freefish.auth.core.dto.UserRoleRelationDto;
import info.freefish.auth.domain.UserDTO;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import info.freefish.common.utils.PageUtils;
import info.freefish.common.utils.Query;

import info.freefish.auth.core.dao.PermissionDao;
import info.freefish.auth.core.entity.PermissionEntity;
import info.freefish.auth.core.service.PermissionService;

@Service("permissionService")
public class PermissionServiceImpl extends ServiceImpl<PermissionDao, PermissionEntity> implements PermissionService {

    @Autowired
    private PermissionDao permissionDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<PermissionEntity> page = this.page(
                new Query<PermissionEntity>().getPage(params),
                new QueryWrapper<PermissionEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public Map<String, List<String>> getPermissionIndex() {
        List<PermissionIndexDto> permissionIndexDtos = permissionDao.perMissionIndexs();
        Map<String, List<PermissionIndexDto>> collect = permissionIndexDtos.stream().collect(Collectors.groupingBy(PermissionIndexDto::getPerm));
        Map<String, List<String>> map = new HashMap<>();
        collect.forEach((k, v) -> {
            List<String> names = v.stream().map(PermissionIndexDto::getName).collect(Collectors.toList());
            map.put(k, names);
        });
        return map;
    }

    @Override
    public Map<String, List<String>> getUserRelations(String name) {
        List<UserRoleRelationDto> userRoleRelations = permissionDao.getUserRelations(name);
        Map<String, List<UserRoleRelationDto>> collect = userRoleRelations.stream().collect(Collectors.groupingBy(UserRoleRelationDto::getUsername));
        val map = new HashMap<String, List<String>>();
        collect.forEach((k, v) -> {
            List<String> names = v.stream().map(UserRoleRelationDto::getRolename).collect(Collectors.toList());
            map.put(k, names);
        });
        return map;
    }

    @Override
    public List<UserDTO> getUserDTOS(String name) {
        List<UserRoleRelationDto> userRoleRelations = permissionDao.getUserRelations(name);
        Map<String, List<UserRoleRelationDto>> collect = userRoleRelations.stream().collect(Collectors.groupingBy(UserRoleRelationDto::getUsername));
        List<UserDTO> userDTOS = new ArrayList<>();
        collect.forEach((k, v) -> {
            List<String> names = v.stream().map(UserRoleRelationDto::getRolename).collect(Collectors.toList());
            userDTOS.add(new UserDTO(Long.parseLong(v.get(0).getId().toString()), v.get(0).getUsername(), v.get(0).getPassword(), v.get(0).getStatus(), names));
        });
        return userDTOS;
    }


}