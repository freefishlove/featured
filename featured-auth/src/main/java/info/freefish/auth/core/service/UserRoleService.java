package info.freefish.auth.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import info.freefish.common.utils.PageUtils;
import info.freefish.auth.core.entity.UserRoleEntity;

import java.util.Map;

/**
 * 用户和角色关联表
 *
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-05-24 15:20:25
 */
public interface UserRoleService extends IService<UserRoleEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

