package info.freefish.auth.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import info.freefish.auth.core.entity.IgnoreUrlsEntity;

public interface IgnoreUrlsService  extends IService<IgnoreUrlsEntity> {

}
