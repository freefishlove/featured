package info.freefish.auth.core.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import info.freefish.auth.core.entity.DeptEntity;
import info.freefish.auth.core.entity.IgnoreUrlsEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 部门表
 * 
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-05-24 15:20:25
 */
@Mapper
public interface IgnoreUrlsDao extends BaseMapper<IgnoreUrlsEntity> {
	
}
