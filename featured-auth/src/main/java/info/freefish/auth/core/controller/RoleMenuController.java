package info.freefish.auth.core.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import info.freefish.auth.core.entity.RoleMenuEntity;
import info.freefish.auth.core.service.RoleMenuService;
import info.freefish.common.utils.PageUtils;
import info.freefish.common.utils.R;



/**
 * 角色和菜单关联表
 *
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-05-24 15:20:25
 */
@RestController
@RequestMapping("core/rolemenu")
public class RoleMenuController {
    @Autowired
    private RoleMenuService roleMenuService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("core:rolemenu:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = roleMenuService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{roleId}")
   // @RequiresPermissions("core:rolemenu:info")
    public R info(@PathVariable("roleId") Long roleId){
		RoleMenuEntity roleMenu = roleMenuService.getById(roleId);

        return R.ok().put("roleMenu", roleMenu);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("core:rolemenu:save")
    public R save(@RequestBody RoleMenuEntity roleMenu){
		roleMenuService.save(roleMenu);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
   // @RequiresPermissions("core:rolemenu:update")
    public R update(@RequestBody RoleMenuEntity roleMenu){
		roleMenuService.updateById(roleMenu);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
   // @RequiresPermissions("core:rolemenu:delete")
    public R delete(@RequestBody Long[] roleIds){
		roleMenuService.removeByIds(Arrays.asList(roleIds));

        return R.ok();
    }

}
