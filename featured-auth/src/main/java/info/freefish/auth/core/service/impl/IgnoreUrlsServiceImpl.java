package info.freefish.auth.core.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import info.freefish.auth.core.dao.DictDao;
import info.freefish.auth.core.dao.IgnoreUrlsDao;
import info.freefish.auth.core.entity.DictEntity;
import info.freefish.auth.core.entity.IgnoreUrlsEntity;
import info.freefish.auth.core.service.DictService;
import info.freefish.auth.core.service.IgnoreUrlsService;
import org.springframework.stereotype.Service;

/**
 * Created By Freefish on 2021/6/4 0004
 */
@Service("ignoreUrlsService")
public class IgnoreUrlsServiceImpl extends ServiceImpl<IgnoreUrlsDao, IgnoreUrlsEntity> implements IgnoreUrlsService {
}
