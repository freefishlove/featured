package info.freefish.auth.core.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import info.freefish.auth.core.entity.DictEntity;
import info.freefish.auth.core.service.DictService;
import info.freefish.common.utils.PageUtils;
import info.freefish.common.utils.R;



/**
 * 字典类型表
 *
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-05-24 15:20:25
 */
@RestController
@RequestMapping("core/dict")
public class DictController {
    @Autowired
    private DictService dictService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("core:dict:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = dictService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
   // @RequiresPermissions("core:dict:info")
    public R info(@PathVariable("id") Long id){
		DictEntity dict = dictService.getById(id);

        return R.ok().put("dict", dict);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("core:dict:save")
    public R save(@RequestBody DictEntity dict){
		dictService.save(dict);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
   // @RequiresPermissions("core:dict:update")
    public R update(@RequestBody DictEntity dict){
		dictService.updateById(dict);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
   // @RequiresPermissions("core:dict:delete")
    public R delete(@RequestBody Long[] ids){
		dictService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
