package info.freefish.auth.core.dao;

import info.freefish.auth.core.entity.DictItemEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 字典数据表
 * 
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-05-24 15:20:25
 */
@Mapper
public interface DictItemDao extends BaseMapper<DictItemEntity> {
	
}
