package info.freefish.auth.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 字典数据表
 * 
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-05-24 15:20:25
 */
@Data
@TableName("sys_dict_item")
public class DictItemEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long id;
	/**
	 * 字典项名称
	 */
	private String name;
	/**
	 * 字典项值
	 */
	private String value;
	/**
	 * 字典编码
	 */
	private String dictCode;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 状态（0 停用 1正常）
	 */
	private Integer status;
	/**
	 * 是否默认（0否 1是）
	 */
	private Integer defaulted;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 创建时间
	 */
	private Date gmtCreate;
	/**
	 * 更新时间
	 */
	private Date gmtModified;

}
