package info.freefish.auth.core.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * Created By Freefish on 2021/5/25 0025
 */
@Data
@AllArgsConstructor
public class PermissionIndexDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private  Integer id;
    private  String perm;
    private  String name;

}
