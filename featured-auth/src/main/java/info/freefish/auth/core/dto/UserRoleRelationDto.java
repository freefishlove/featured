package info.freefish.auth.core.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created By Freefish on 2021/5/25 0025
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRoleRelationDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private  Integer id;
    private  String username;
    private  String password;
    private  String rolename;
    private Integer status;
}
