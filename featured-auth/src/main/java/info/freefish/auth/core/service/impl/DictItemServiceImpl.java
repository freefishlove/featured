package info.freefish.auth.core.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import info.freefish.common.utils.PageUtils;
import info.freefish.common.utils.Query;

import info.freefish.auth.core.dao.DictItemDao;
import info.freefish.auth.core.entity.DictItemEntity;
import info.freefish.auth.core.service.DictItemService;


@Service("dictItemService")
public class DictItemServiceImpl extends ServiceImpl<DictItemDao, DictItemEntity> implements DictItemService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<DictItemEntity> page = this.page(
                new Query<DictItemEntity>().getPage(params),
                new QueryWrapper<DictItemEntity>()
        );

        return new PageUtils(page);
    }

}