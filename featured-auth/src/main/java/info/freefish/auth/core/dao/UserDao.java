package info.freefish.auth.core.dao;

import info.freefish.auth.core.dto.UserRoleRelationDto;
import info.freefish.auth.core.entity.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户信息表
 * 
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-05-24 15:20:25
 */
//@Mapper
//public interface UserDao extends BaseMapper<UserEntity> {
//
//      List<UserRoleRelationDto> getUserRoleRelations();
//}


/**
 * 用户和角色关联表
 *
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-05-24 15:20:25
 */
@Mapper
public interface UserDao extends BaseMapper<UserEntity> {
    List<UserRoleRelationDto> getUserRoleRelations();
}