package info.freefish.auth.core.dao;

import info.freefish.auth.core.entity.DictEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 字典类型表
 * 
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-05-24 15:20:25
 */
@Mapper
public interface DictDao extends BaseMapper<DictEntity> {
	
}
