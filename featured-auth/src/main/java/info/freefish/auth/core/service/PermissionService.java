package info.freefish.auth.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import info.freefish.auth.domain.UserDTO;
import info.freefish.common.utils.PageUtils;
import info.freefish.auth.core.entity.PermissionEntity;

import java.util.List;
import java.util.Map;

/**
 * 权限表
 *
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-05-24 15:20:25
 */
public interface PermissionService extends IService<PermissionEntity> {

    PageUtils queryPage(Map<String, Object> params);

    Map<String , List<String>> getPermissionIndex();



    Map<String, List<String>> getUserRelations(String name);

    List<UserDTO> getUserDTOS(String name);

}

