package info.freefish.auth.core.dao;

import info.freefish.auth.core.entity.RolePermissionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 角色权限表
 * 
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-05-24 15:20:25
 */
@Mapper
public interface RolePermissionDao extends BaseMapper<RolePermissionEntity> {
	
}
