package info.freefish.auth.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 权限表
 * 
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-05-24 15:20:25
 */
@Data
@TableName("sys_permission")
public class PermissionEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long id;
	/**
	 * 权限名称
	 */
	private String name;
	/**
	 * 权限标识
	 */
	private String perm;
	/**
	 * 请求方法类型（POST/PUT/DELETE/PATCH）
	 */
	private String method;
	/**
	 * 权限类型 1-路由权限 2-按钮权限
	 */
	private Integer type;
	/**
	 * 菜单模块ID

	 */
	private Long moduleId;
	/**
	 * 
	 */
	private Date gmtCreate;
	/**
	 * 
	 */
	private Date gmtModified;

}
