package info.freefish.auth.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * Created By Freefish on 2021/6/4 0004
 */
@Data
@TableName("sys_ignore_urls")
public class IgnoreUrlsEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId
    private Long id;
    /**
     * 菜单名称
     */
    private String url;
}
