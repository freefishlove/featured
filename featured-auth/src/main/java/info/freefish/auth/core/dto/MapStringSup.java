package info.freefish.auth.core.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created By Freefish on 2021/5/25 0025
 */
@Data
public class MapStringSup implements Serializable {
    private static final long serialVersionUID = 1L;
    private  String perm;
    private  String name;
}
