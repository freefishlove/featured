package info.freefish.auth.core.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import info.freefish.auth.core.service.UserServiceSelf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import info.freefish.auth.core.entity.UserEntity;
import info.freefish.common.utils.PageUtils;
import info.freefish.common.utils.R;



/**
 * 用户信息表
 *
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-05-24 15:20:25
 */
@RestController
@RequestMapping("core/user")
public class UserController {
    @Autowired
    private UserServiceSelf userServiceSelf;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("core:user:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = userServiceSelf.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
   // @RequiresPermissions("core:user:info")
    public R info(@PathVariable("id") Long id){
		UserEntity user = userServiceSelf.getById(id);

        return R.ok().put("user", user);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("core:user:save")
    public R save(@RequestBody UserEntity user){
		userServiceSelf.save(user);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
   // @RequiresPermissions("core:user:update")
    public R update(@RequestBody UserEntity user){
		userServiceSelf.updateById(user);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
   // @RequiresPermissions("core:user:delete")
    public R delete(@RequestBody Long[] ids){
		userServiceSelf.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
