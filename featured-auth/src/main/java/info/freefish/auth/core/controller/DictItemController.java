package info.freefish.auth.core.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import info.freefish.auth.core.entity.DictItemEntity;
import info.freefish.auth.core.service.DictItemService;
import info.freefish.common.utils.PageUtils;
import info.freefish.common.utils.R;



/**
 * 字典数据表
 *
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-05-24 15:20:25
 */
@RestController
@RequestMapping("core/dictitem")
public class DictItemController {
    @Autowired
    private DictItemService dictItemService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("core:dictitem:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = dictItemService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
   // @RequiresPermissions("core:dictitem:info")
    public R info(@PathVariable("id") Long id){
		DictItemEntity dictItem = dictItemService.getById(id);

        return R.ok().put("dictItem", dictItem);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("core:dictitem:save")
    public R save(@RequestBody DictItemEntity dictItem){
		dictItemService.save(dictItem);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
   // @RequiresPermissions("core:dictitem:update")
    public R update(@RequestBody DictItemEntity dictItem){
		dictItemService.updateById(dictItem);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
   // @RequiresPermissions("core:dictitem:delete")
    public R delete(@RequestBody Long[] ids){
		dictItemService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
