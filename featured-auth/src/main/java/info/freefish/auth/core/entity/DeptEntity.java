package info.freefish.auth.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 部门表
 * 
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-05-24 15:20:25
 */
@Data
@TableName("sys_dept")
public class DeptEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long id;
	/**
	 * 部门名称
	 */
	private String name;
	/**
	 * 父节点id
	 */
	private Integer parentId;
	/**
	 * 父节点id路径
	 */
	private String treePath;
	/**
	 * 显示顺序
	 */
	private Integer sort;
	/**
	 * 负责人
	 */
	private String leader;
	/**
	 * 联系电话
	 */
	private String mobile;
	/**
	 * 邮箱地址
	 */
	private String email;
	/**
	 * 部门状态（0正常 1停用）
	 */
	private Integer status;
	/**
	 * 删除标志（0存在 1删除）
	 */
	private Integer deleted;
	/**
	 * 创建时间
	 */
	private Date gmtCreate;
	/**
	 * 更新时间
	 */
	private Date gmtModified;

}
