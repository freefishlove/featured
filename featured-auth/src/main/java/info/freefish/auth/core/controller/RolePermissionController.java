package info.freefish.auth.core.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import info.freefish.auth.core.entity.RolePermissionEntity;
import info.freefish.auth.core.service.RolePermissionService;
import info.freefish.common.utils.PageUtils;
import info.freefish.common.utils.R;



/**
 * 角色权限表
 *
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-05-24 15:20:25
 */
@RestController
@RequestMapping("core/rolepermission")
public class RolePermissionController {
    @Autowired
    private RolePermissionService rolePermissionService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("core:rolepermission:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = rolePermissionService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{roleId}")
   // @RequiresPermissions("core:rolepermission:info")
    public R info(@PathVariable("roleId") Long roleId){
		RolePermissionEntity rolePermission = rolePermissionService.getById(roleId);

        return R.ok().put("rolePermission", rolePermission);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("core:rolepermission:save")
    public R save(@RequestBody RolePermissionEntity rolePermission){
		rolePermissionService.save(rolePermission);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
   // @RequiresPermissions("core:rolepermission:update")
    public R update(@RequestBody RolePermissionEntity rolePermission){
		rolePermissionService.updateById(rolePermission);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
   // @RequiresPermissions("core:rolepermission:delete")
    public R delete(@RequestBody Long[] roleIds){
		rolePermissionService.removeByIds(Arrays.asList(roleIds));

        return R.ok();
    }

}
