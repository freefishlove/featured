package info.freefish.auth.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import info.freefish.auth.core.entity.UserEntity;
import info.freefish.auth.domain.UserDTO;
import info.freefish.auth.vo.UserVo;
import info.freefish.common.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * 用户信息表
 *
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-05-24 15:20:25
 */
public interface UserServiceSelf extends IService<UserEntity> {

    PageUtils queryPage(Map<String, Object> params);

    Boolean save(UserVo user);

}

