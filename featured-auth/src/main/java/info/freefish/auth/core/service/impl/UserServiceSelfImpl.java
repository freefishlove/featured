package info.freefish.auth.core.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import info.freefish.auth.core.dao.UserDao;
import info.freefish.auth.core.dto.UserRoleRelationDto;
import info.freefish.auth.core.entity.UserEntity;
import info.freefish.auth.core.entity.UserRoleEntity;
import info.freefish.auth.core.service.UserRoleService;
import info.freefish.auth.core.service.UserServiceSelf;
import info.freefish.auth.domain.UserDTO;
import info.freefish.auth.vo.UserVo;
import info.freefish.common.utils.PageUtils;
import info.freefish.common.utils.Query;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;


@Service("userServiceSelf")
@Slf4j
public class UserServiceSelfImpl extends ServiceImpl<UserDao, UserEntity> implements UserServiceSelf {

    @Autowired
    UserDao userDao;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    UserRoleService userRoleService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<UserEntity> page = this.page(
                new Query<UserEntity>().getPage(params),
                new QueryWrapper<UserEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean save(UserVo user) {
        try {
            UserEntity userEntity = new UserEntity();
            BeanUtils.copyProperties(user, userEntity);
            userEntity.setGmtCreate(new Date());
            userEntity.setGmtModified(new Date());
            userEntity.setStatus(0);
            userEntity.setPassword(passwordEncoder.encode(user.getPassword()));
            this.save(userEntity);
            if (user.getRoleIds() != null) {
                Collection<UserRoleEntity> userRoleEntities = user.getRoleIds().stream().map(p -> new UserRoleEntity(null,userEntity.getId(), p)).collect(Collectors.toList());
                userRoleService.saveBatch(userRoleEntities);
            }
            return true;
        } catch (Exception e) {
            log.error("Save user user_role failed" + e.getMessage());
            throw e;
        }
    }

}