package info.freefish.auth.core.dao;

import info.freefish.auth.core.dto.PermissionIndexDto;
import info.freefish.auth.core.dto.UserRoleRelationDto;
import info.freefish.auth.core.entity.PermissionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 权限表
 * 
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-05-24 15:20:25
 */
@Mapper
public interface PermissionDao extends BaseMapper<PermissionEntity> {

    List<PermissionIndexDto> perMissionIndexs();

    List<UserRoleRelationDto> getUserRelations(@Param("name") String name);
}
