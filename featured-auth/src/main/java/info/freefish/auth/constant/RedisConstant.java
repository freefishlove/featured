package info.freefish.auth.constant;

/**
 * Redis常量
 */
public class RedisConstant {

    public static final String RESOURCE_ROLES_MAP = "AUTH:RESOURCE_ROLES_MAP";
    public static final String IGNORE_URLS = "AUTH:IGNORE_URLS";

}
