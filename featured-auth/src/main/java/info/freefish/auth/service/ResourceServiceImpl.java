package info.freefish.auth.service;

import com.alibaba.fastjson.JSON;
import info.freefish.auth.constant.RedisConstant;
import info.freefish.auth.core.dao.PermissionDao;
import info.freefish.auth.core.dto.PermissionIndexDto;
import info.freefish.auth.core.entity.IgnoreUrlsEntity;
import info.freefish.auth.core.service.IgnoreUrlsService;
import info.freefish.auth.core.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 资源与角色匹配关系管理业务类
 * 加载权限与角色关系业务
 * 加载白名单url
 */
@Service
public class ResourceServiceImpl {
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private IgnoreUrlsService ignoreUrlsService;

    @PostConstruct
    public void initData() {
        //记载路径和用户匹配
        Map<String, List<String>> permissionIndex = permissionService.getPermissionIndex();
        redisTemplate.opsForHash().putAll(RedisConstant.RESOURCE_ROLES_MAP, permissionIndex);

        //配置忽略url
        List<IgnoreUrlsEntity> list = ignoreUrlsService.list();
        String s = JSON.toJSONString(list);
        stringRedisTemplate.opsForValue().set(RedisConstant.IGNORE_URLS, s);
    }
}
