package info.freefish.auth.controller;

import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.KeyPair;
import java.security.interfaces.RSAPublicKey;
import java.util.Map;


/**
 * 获取RSA公钥接口
 */
@RestController
public class KeyPairController {

    @Autowired
    private KeyPair keyPair;

    @GetMapping("/rsa/publicKey")
    public Map<String, Object> getKey() {
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        RSAKey key = new RSAKey.Builder(publicKey).build();
        return new JWKSet(key).toJSONObject();
    }


    //    @Cacheable(cacheNames = {"CacheTest"},key = "#root.method.name")  //当前方法的可以缓存  如果有这个缓存 方法不调用
    //@CacheEvict(cacheNames = {"CacheTest"}, key = "'cacheTest'")  失效模式时候使用  修改就自己删除
    @GetMapping("/test/cache")
    public String cacheTest() {
        return "cache test ni cam you up change";
    }

    @Cacheable(cacheNames = {"CacheTest"}, key = "#root.method.name")
    @GetMapping("/test/save")
    public String cacheSave() {
        return " cacheSave ni cam you up change";
    }


    public static void main(String[] args) {
        String a = "aa";
        String b = new String("aa");
        System.out.println(a == b);
        System.out.println(a.equals(b));
    }

}
