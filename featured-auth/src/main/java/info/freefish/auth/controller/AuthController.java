package info.freefish.auth.controller;

import cn.hutool.core.util.StrUtil;
import com.alibaba.druid.support.json.JSONUtils;
import com.alibaba.fastjson.JSON;
import com.nimbusds.jose.JWSObject;
import info.freefish.auth.api.CommonResult;
import info.freefish.auth.constant.AuthConstants;
import info.freefish.auth.constant.MessageConstant;
import info.freefish.auth.core.dao.PermissionDao;
import info.freefish.auth.core.service.UserServiceSelf;
import info.freefish.auth.domain.Oauth2TokenDto;
import info.freefish.auth.vo.UserVo;
import info.freefish.common.exception.BizCodeEnume;
import info.freefish.common.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.text.ParseException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 自定义Oauth2获取令牌接口
 */
@RestController
@RequestMapping("/oauth")
public class AuthController {

    @Autowired
    private TokenEndpoint tokenEndpoint;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private UserServiceSelf userServiceSelf;

    /**
     * Oauth2登录认证
     */
    @RequestMapping(value = "/token", method = RequestMethod.POST)
    public CommonResult<Oauth2TokenDto> postAccessToken(Principal principal, @RequestParam Map<String, String> parameters) throws HttpRequestMethodNotSupportedException {
        if (!parameters.containsKey("client_id") || StrUtil.isEmptyOrUndefined(parameters.get("client_id"))) {
            parameters.put("client_id", "common");
        }
        if (!parameters.containsKey("grant_type") || StrUtil.isEmptyOrUndefined(parameters.get("grant_type"))) {
            parameters.put("grant_type", "password");
        }
        if (!parameters.containsKey("client_secret") || StrUtil.isEmptyOrUndefined(parameters.get("client_secret"))) {
            parameters.put("client_secret", "123456");
        }
        OAuth2AccessToken oAuth2AccessToken = tokenEndpoint.postAccessToken(principal, parameters).getBody();
        Oauth2TokenDto oauth2TokenDto = Oauth2TokenDto.builder()
                .token(oAuth2AccessToken.getValue())
                .refreshToken(oAuth2AccessToken.getRefreshToken().getValue())
                .expiresIn(oAuth2AccessToken.getExpiresIn())
                .tokenHead("Bearer ").build();

        return CommonResult.success(oauth2TokenDto);
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public R register(@RequestBody UserVo user) {
        Boolean save = userServiceSelf.save(user);
        if (save)
            return R.ok();
        else
           return R.error().put(String.valueOf(BizCodeEnume.USER_EXIT_OR_OTHER_EXCEPTION.getCode()) ,BizCodeEnume.USER_EXIT_OR_OTHER_EXCEPTION.getMsg());
    }


    @GetMapping("/logout")
    public CommonResult logout(HttpServletRequest request) throws ParseException {
        String token = request.getHeader(AuthConstants.JWT_PAYLOAD_KEY);
        String realToken = token.replace("Bearer ", "");
        JWSObject jwsObject = JWSObject.parse(realToken);
        //获取Payload  实体信息部分
        String userStr = jwsObject.getPayload().toString();


        String jti = (String) JSON.parseObject(userStr).get(AuthConstants.JWT_JTI);
        Long exp = JSON.parseObject(userStr).getLong(AuthConstants.JWT_EXP);
        long currentTimeSeconds = System.currentTimeMillis() / 1000;

        if (exp < currentTimeSeconds) { // token已过期
            return CommonResult.failed(MessageConstant.CREDENTIALS_EXPIRED);
        }
        redisTemplate.opsForValue().set(AuthConstants.TOKEN_BLACKLIST_PREFIX + jti, null, (exp - currentTimeSeconds), TimeUnit.SECONDS);
        return CommonResult.success(MessageConstant.LOGIN_OUT_SUCCESS);
    }

}
