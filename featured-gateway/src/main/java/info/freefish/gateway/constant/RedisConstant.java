package info.freefish.gateway.constant;


public class RedisConstant {

    public static final String RESOURCE_ROLES_MAP = "AUTH:RESOURCE_ROLES_MAP";
    //public static final String JWT_TOKEN_HEADER = "AUTH:RESOURCE_ROLES_MAP";
    public static final String PERMISSION_ROLES_KEY = "AUTH:PERMISSION:ROLES";
    public static final String IGNORE_URLS = "AUTH:IGNORE_URLS";
}
