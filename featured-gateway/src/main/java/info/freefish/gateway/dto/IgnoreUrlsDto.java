package info.freefish.gateway.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created By Freefish on 2021/6/4 0004
 */
@Data
public class IgnoreUrlsDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    private Long id;
    /**
     * 菜单名称
     */
    private String url;
}
