package info.freefish.gateway.service.impl;

import com.alibaba.fastjson.JSON;
import info.freefish.gateway.config.IgnoreUrlsConfig;
import info.freefish.gateway.constant.RedisConstant;
import info.freefish.gateway.dto.IgnoreUrlsDto;
import info.freefish.gateway.service.UrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("urlService")
public class UrlServiceImpl implements UrlService {
    @Autowired
    private IgnoreUrlsConfig ignoreUrlsConfig;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public List<String> getIgnoreUrls() {

        List<String> ignoreUrls = ignoreUrlsConfig.getUrls();
        List<String> realIgnoreUrls=new ArrayList<>();
        String s = stringRedisTemplate.opsForValue().get(RedisConstant.IGNORE_URLS);
        List<IgnoreUrlsDto> ignoreUrlsDtos = JSON.parseArray(s, IgnoreUrlsDto.class);
        List<String> collect = ignoreUrlsDtos.stream().map(IgnoreUrlsDto::getUrl).collect(Collectors.toList());
        realIgnoreUrls.addAll(ignoreUrls);
        realIgnoreUrls.addAll(collect);
        return realIgnoreUrls;

    }
}
