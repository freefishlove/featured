package info.freefish.gateway.service;

import java.util.List;

public interface UrlService {

    List<String> getIgnoreUrls();
}
