package info.freefish.gateway.api;

public interface AuthConstants {



    /**
     * JWT载体key
     */
    String JWT_PAYLOAD_KEY = "Authorization";

    /**
     * JWT ID 唯一标识
     */
    String JWT_JTI = "jti";

    /**
     * JWT ID 唯一标识
     */
    String JWT_EXP = "exp";

    /**
     * Redis缓存权限规则key permission
     */
    String PERMISSION_ROLES_KEY = "AUTH:PERMISSION:ROLES";

    /**
     * 黑名单token前缀
     */
    String TOKEN_BLACKLIST_PREFIX = "AUTH:TOKEN:BLACKLIST:";

   
}
