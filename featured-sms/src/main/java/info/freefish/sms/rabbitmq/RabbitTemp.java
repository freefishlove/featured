package info.freefish.sms.rabbitmq;

import com.rabbitmq.client.Channel;
import info.freefish.sms.domain.UserDTO;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * Created By Freefish on 2021/6/8 0008
 */
@Service
public class RabbitTemp {


    @RabbitListener(queues = {"hello-java-queue"})
    public void receive(Message message, Channel channel) {
        System.out.println("接收到：" + message + "类型为：");

        //第二个代表是不是批量模式
        try {
            long deliveryTag = message.getMessageProperties().getDeliveryTag();
            if (deliveryTag % 2 == 0)
                channel.basicAck(deliveryTag, false);
            else
                channel.basicNack(deliveryTag, false, true);//第三个参数为true重新入队

        } catch (Exception e) {
        }
    }

    @RabbitListener(queues = {"hello-java-queue"})
    public void receive1(Message message, UserDTO userDTO, Channel channel) {
        System.out.println("接收到UserDto：" + userDTO + "类型为：");
        try {
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
