package info.freefish.sms.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created By Freefish on 2021/5/13 0013
 */
@RestController
@RequestMapping("/sms")
public class TestController {

    @Value("${server.port}")
    private String port;

    @GetMapping("/hello")
    public String test() {
        return "hello world Feign" + port;
    }


    @GetMapping("/god")
    public String god() {
        return "hello god";
    }
}
