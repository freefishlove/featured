package info.freefish.sms.controller;

import info.freefish.sms.domain.UserDTO;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * 测试接口
 * Created by macro on 2020/6/19.
 */
@RestController
public class HelloController {
    @Value("${server.port}")
    private String port;

    @GetMapping("/hello")
    public String hello() {
        return "Hello World." + port;
    }

    @Autowired
    RabbitTemplate rabbitTemplate;

    @GetMapping("sendMq")
    public void sendtest() {
        UserDTO userDTO = new UserDTO();
        userDTO.setUsername("xuwei");
        userDTO.setPassword("11111111");
        for (int i = 0; i < 10; i++) {
            rabbitTemplate.convertAndSend("hello-java-exchange", "hello-java-route1", userDTO, new CorrelationData(UUID.randomUUID().toString()));
        }
    }
}
