package info.freefish.sms.testRefresh;

import info.freefish.sms.domain.UserDTO;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@WebFilter(urlPatterns = "/get/hello", filterName = "myFilter")
public class myFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("Filter==============================》》》》》》》》》");

        Filter.super.init(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("Filter==============================》》》》》》》》》");
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
        System.out.println("Filter==============================》》》》》》》》》");

        Filter.super.destroy();
    }


    public static void main(String[] args) {

        HashMap hashMap = new HashMap();
        hashMap.put("1", "1");
        hashMap.put("2", "2");
        hashMap.put("1", "4");

        Properties properties = new Properties();

        Queue arrayDeque = new ArrayDeque();
        arrayDeque.add("add");
        arrayDeque.add("add1");

        arrayDeque.poll();

    }

    static class Dog {
        public Dog(String name) {
            this.name = name;
        }

        private String name;

        @Override
        public int hashCode() {
            return 100;
        }
    }


    static class Employee {

        private String name;
        private Integer age;
        private Bir bir;

        public Employee(String name, Integer age, Bir bir) {
            this.name = name;
            this.age = age;
            this.bir = bir;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Employee)) return false;
            Employee employee = (Employee) o;
            return name.equals(employee.name) && age.equals(employee.age) && bir.equals(employee.bir);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, age, bir);
        }
    }

    static class Bir {
        private Integer year;
        private Integer month;
        private Integer day;

        public Bir(Integer year, Integer month, Integer day) {
            this.year = year;
            this.month = month;
            this.day = day;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Bir)) return false;
            Bir bir = (Bir) o;
            return year.equals(bir.year) && month.equals(bir.month) && day.equals(bir.day);
        }

        @Override
        public int hashCode() {
            return Objects.hash(year, month, day);
        }
    }

}
