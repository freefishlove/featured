package info.freefish.sms.testRefresh.event;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * Created By Freefish on 2021/9/3 0003
 */
@Component
public class EmailListener implements ApplicationListener<EmailEvent> {
    @Override
    public void onApplicationEvent(EmailEvent emailEvent) {
        System.out.println(emailEvent.getMail());
    }
}
