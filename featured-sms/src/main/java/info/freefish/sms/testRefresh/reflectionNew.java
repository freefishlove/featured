package info.freefish.sms.testRefresh;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class reflectionNew {
    //通过反射创建对象
    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, NoSuchFieldException {
        Class<?> c = Class.forName("info.freefish.sms.testRefresh.User");

        User user = (User) c.newInstance();
        System.out.println(user);

        Constructor constructor = c.getDeclaredConstructor(String.class, String.class, Integer.class);
        User user1 = (User)constructor.newInstance("许伟", "xuwei", 25);
        System.out.println(user1);

        //反射获取方法
        Method setName = c.getDeclaredMethod("setName", String.class);
        Object hardhat = setName.invoke(user1, "hardhat");
        String name = user1.getName();
        System.out.println(name);

        //反射操作属性
        User user2 = (User) c.newInstance();
        Field name1 = c.getDeclaredField("name");

        name1.setAccessible(true);//取消检查
        name1.set(user2,"setsetstetss");

        System.out.println(user2.getName());



    }
}
