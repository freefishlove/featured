package info.freefish.sms.testRefresh.event;

import org.springframework.context.ApplicationEvent;

/**
 * Created By Freefish on 2021/9/3 0003
 *
 * @author Administrator
 */
public class EmailEvent extends ApplicationEvent {

    public EmailEvent(Object source) {
        super(source);
    }

    public String getMail() {
        return "hello world";
    }
}
