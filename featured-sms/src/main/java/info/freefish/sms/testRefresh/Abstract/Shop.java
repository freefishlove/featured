package info.freefish.sms.testRefresh.Abstract;

import java.util.concurrent.CompletableFuture;

public class Shop extends AbstractShop {

    @Override
    public Integer getMoneyOfOther(Integer money) {
        return (money) / 6;
    }

    public static void main(String[] args) {

        CompletableFuture<Integer> integerCompletableFuture = CompletableFuture.supplyAsync(() -> 11);
        CompletableFuture<Void> voidCompletableFuture = integerCompletableFuture.thenAccept((p) -> {
            System.out.println(p);
        });
    }
}
