package info.freefish.sms.testRefresh;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class thread {
    public static class AirCondition {

        private volatile int num = 0;

        public synchronized void increment() throws InterruptedException {
            while (num != 0) {
                this.wait();
            }
            System.out.println(Thread.currentThread().getName() + "=====>" + num++);
            this.notifyAll();
        }

        public synchronized void decrement() throws InterruptedException {
            while (num != 1) {
                this.wait();
            }
            System.out.println(Thread.currentThread().getName() + "=====>" + num--);
            this.notifyAll();
        }
    }

    public static class AirConditionLock {

        private volatile int num = 0;

        private final Lock lock = new ReentrantLock();
        private final Condition condition = lock.newCondition();

        public void increment() throws InterruptedException {
            lock.lock();
            try {
                while (num != 0) {
                    condition.await();
                }
                System.out.println(Thread.currentThread().getName() + "=====>" + num++);
                condition.signalAll();
            } finally {
                lock.unlock();
            }

        }

        public void decrement() throws InterruptedException {
            lock.lock();
            try {
                while (num != 1) {
                    condition.await();
                }
                System.out.println(Thread.currentThread().getName() + "=====>" + num--);
                condition.signalAll();
            } finally {
                lock.unlock();
            }

        }
    }

//    public static void main(String[] args) {
//        AirConditionLock airCondition = new AirConditionLock();
//
//        new Thread(() -> {
//            try {
//                for (int i = 0; i < 10; i++) {
//                    airCondition.increment();
//                }
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }, "A").start();
//        new Thread(() -> {
//            try {
//                for (int i = 0; i < 10; i++) {
//                    airCondition.increment();
//                }
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }, "B").start();
//        new Thread(() -> {
//            try {
//                for (int i = 0; i < 10; i++) {
//                    airCondition.decrement();
//                }
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }, "C").start();
//        new Thread(() -> {
//            try {
//                for (int i = 0; i < 10; i++) {
//                    airCondition.decrement();
//                }
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }, "D").start();
//    }


    public static class Resource {
        private int num = 1;
        private Lock lock = new ReentrantLock();
        private Condition condition1 = lock.newCondition();
        private Condition condition2 = lock.newCondition();
        private Condition condition3 = lock.newCondition();

        public void start() {
            HashMap<Object, Object> objectObjectHashMap = new HashMap<>();

            new CopyOnWriteArrayList();
            lock.lock();
            try {

                while (num != 1) {
                    condition1.wait();
                }
                for (int i = 0; i < 1; i++) {
                    System.out.println("AAAAAA" + Thread.currentThread().getName());
                }
                num = 2;
                condition2.signal();


                while (num != 2) {
                    condition2.wait();
                }
                for (int i = 0; i < 2; i++) {
                    System.out.println("BBBBBB" + Thread.currentThread().getName());
                }
                num = 3;
                condition3.signal();


                while (num != 3) {
                    condition1.wait();
                }
                for (int i = 0; i < 3; i++) {
                    System.out.println("CCCCCCC" + Thread.currentThread().getName());
                }
                num = 1;
                condition1.signal();

            } catch (Exception exception) {

            } finally {
                lock.unlock();
            }
        }
    }

    public static void main(String[] args) {
        Resource resource = new Resource();
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                resource.start();
            }
        }).start();
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                resource.start();
            }
        }).start();
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                resource.start();
            }
        }).start();
    }

}
