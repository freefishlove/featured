package info.freefish.sms.testRefresh;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class testReFlush {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchFieldException, NoSuchMethodException {
        Class<?> c = Class.forName("info.freefish.sms.testRefresh.User");

        System.out.println(c.getName());
        System.out.println(c.getSimpleName());

        Field[] fields1 = c.getFields();//获得共有属性
        //获得类的属性
        Field[] fields = c.getDeclaredFields();//找到全部属性 私有共有
        for (Field field : fields) {
            System.out.println(field);
        }

//        System.out.println(c.getField("name"));
        System.out.println(c.getDeclaredField("name")); //获得属性   可以使私有的

        Method[] methods = c.getMethods();//获本类和 父类的public方法
        for (Method method : methods) {
            System.out.println(method);
        }
        System.out.println("=======================================");
        Method[] methods1 = c.getDeclaredMethods(); //获取本类的所有  方法
        for (Method method : methods1) {
            System.out.println(method);
        }

        //获取指定的方法
        System.out.println(c.getMethod("getName", null));
        System.out.println(c.getMethod("setName", String.class));



    }
}
