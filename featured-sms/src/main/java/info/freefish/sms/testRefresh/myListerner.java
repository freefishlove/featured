package info.freefish.sms.testRefresh;


import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@Component
public class myListerner implements HttpSessionListener {
    public void sessionCreated(HttpSessionEvent se) {
        System.out.println("session  sessionCreated================================");
    }

    public void sessionDestroyed(HttpSessionEvent se) {
        System.out.println("session  sessionDestroyed================================");
    }
}
