package info.freefish.sms.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created By Freefish on 2021/6/9 0009
 */
@Component
@Slf4j
public class RabbitConfig {
    @Autowired
    RabbitTemplate rabbitTemplate;

    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    /**
     * 定制 rabbitTemplate
     * 主要此客户端发送消息Broke代理收到消息就会触发 ACK为true
     */
    @PostConstruct
    public void initRabbitTemplate() {
        rabbitTemplate.setConfirmCallback(new RabbitTemplate.ConfirmCallback() {
            /**
             *
             * @param correlationData 当前消息唯一关联数据（唯一Id）
             * @param ack 是否成功收到
             * @param cause 失败的原因
             */
            @Override
            public void confirm(CorrelationData correlationData, boolean ack, String cause) {
                if (!ack) {
                    log.warn("send message failed: " + cause + correlationData.toString());
                }
            }
        });
    }

    /**
     * 定制 rabbitTemplate 到达队列消息回调
     */
    @PostConstruct  //构造器创建完成调用
    public void initRabbitTemplateReturn() {
        rabbitTemplate.setReturnCallback(new RabbitTemplate.ReturnCallback() {
            //消息没有投递给制定的Queue 才会触发回调

            /**
             *
             * @param message  投递失败的信息的详细信息
             * @param replyCode  回复的状态码
             * @param replyText 回复的内容
             * @param exchange  发哪个交换机的
             * @param routerKey  哪个路由键
             */
            @Override
            public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routerKey) {
                log.warn("Fail Message [" + message + "]" + "\treplyCode: " + replyCode + "\treplyText:" + replyText +
                        "\texchange:" + exchange + "\trouterKey:" + routerKey);

            }
        });
    }
}
