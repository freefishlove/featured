package info.freefish;

import com.rabbitmq.client.*;
import info.freefish.utils.RabbitMqUtils;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created By Freefish on 2021/6/3 0003
 */
public class Customer1 {

    public static void main(String[] args) throws IOException, TimeoutException {
        //获取连接对象
        Connection connection = RabbitMqUtils.getConnection();
        //获取连接channel
        Channel channel = connection.createChannel();
        channel.queueDeclare("testHello", false, false, false, null);
        //消费消息 1. 对垒名称  2.自动确认 3. 回调
        channel.basicConsume("testHello", true, new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println("接收到：" + new String(body));
            }
        });
    }
}
