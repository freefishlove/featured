package info.freefish;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import info.freefish.utils.RabbitMqUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Created By Freefish on 2021/6/7 0007
 */
public class Provider {
    public static void main(String[] args) throws IOException {
        Connection connection = RabbitMqUtils.getConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare("test_exchange","fanout");
        channel.basicPublish("test_exchange","",null,"fanout".getBytes(StandardCharsets.UTF_8));

        RabbitMqUtils.close(channel,connection);

    }
}
