package info.freefish;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.MessageProperties;
import info.freefish.utils.RabbitMqUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

/**
 * Created By Freefish on 2021/6/3 0003
 */
public class Test {

    @org.junit.jupiter.api.Test
    public void TestSendMessage() throws IOException, TimeoutException {
        //获取连接对象
        Connection connection = RabbitMqUtils.getConnection();
        //获取连接channel
        Channel channel = connection.createChannel();
        //通道和消息队列绑定
        /**
         * 参数一 队列名称  没有的话会自动创建
         * 参数二 定义对列是都持久化
         * 参数三  是否独占队列  其他不能再用  true 代表独占 其他连接不可用
         * 参数四  autodelete  是否自动删除队列 在消费完成之后
         * 参数五  额外的参数
         */
        channel.queueDeclare("testHello", false, false, false, null);

        for (int i = 0; i < 20; i++) {
            //发布消息 参数一 交换机名称  参数二 队列名称  参数三 额外设置  参数四 具体消息类型
            channel.basicPublish("", "testHello", MessageProperties.PERSISTENT_TEXT_PLAIN, ("hello world" + i).getBytes(StandardCharsets.UTF_8));
        }

        RabbitMqUtils.close(channel,connection);
    }
}
