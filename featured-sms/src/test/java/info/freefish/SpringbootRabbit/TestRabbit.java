package info.freefish.SpringbootRabbit;

import info.freefish.sms.SmsApplication;
import info.freefish.sms.domain.UserDTO;
import info.freefish.sms.testRefresh.event.EmailEvent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created By Freefish on 2021/6/8 0008
 */
@SpringBootTest(classes = SmsApplication.class)
@RunWith(SpringRunner.class)
public class TestRabbit {


    @Autowired
    AmqpAdmin amqpAdmin;

    @Test
    public void testAdmin() {
        /**
         *   public DirectExchange(String name, boolean durable, boolean autoDelete, Map<String, Object> arguments) {
         *         super(name, durable, autoDelete, arguments);
         *     }
         */
        //创建交换机
        DirectExchange directExchange = new DirectExchange("hello-java-exchange", true, false);
        amqpAdmin.declareExchange(directExchange);
        //创建队列
        Queue queue = new Queue("hello-java-queue", true, false, false, null);
        amqpAdmin.declareQueue(queue);
        //创建交换机
        Binding binding = new Binding("hello-java-queue", Binding.DestinationType.QUEUE, "hello-java-exchange", "hello-java-route", null);
        amqpAdmin.declareBinding(binding);
    }

    @Test
    public void sendtest() {
        UserDTO userDTO = new UserDTO();
        userDTO.setUsername("xuwei");
        userDTO.setPassword("11111111");
        for (int i = 0; i < 10; i++) {
            rabbitTemplate.convertAndSend("hello-java-exchange", "hello-java-route", userDTO);
        }
    }

    @Autowired
    ApplicationContext applicationContext;

    @Test
    public void sendMa() {
        applicationContext.publishEvent(new EmailEvent("testSource"));
    }


    @Autowired
    RabbitTemplate rabbitTemplate;

    //hello world 直连
    @Test
    public void test1() {
        rabbitTemplate.convertAndSend("hello_springboot", "我在发送数据通过Springboot");
    }

    //一对多  消息被多个监听

    @Test
    public void test2() {
        for (int i = 0; i < 20; i++) {
            rabbitTemplate.convertAndSend("work", "Work模型:" + i);
        }
    }

    //fanout 广播
    @Test
    public void test3() {
        for (int i = 0; i < 20; i++) {
            rabbitTemplate.convertAndSend("logs", "", "fanout模型:" + i);
        }
    }

    //fanout Direct
    @Test
    public void test4() {
        for (int i = 0; i < 20; i++) {
            if (i % 2 == 0)
                rabbitTemplate.convertAndSend("directs", "err", "Direct模型:" + i);
            else
                rabbitTemplate.convertAndSend("directs", "err", "Direct模型:" + i);

        }
    }


}
