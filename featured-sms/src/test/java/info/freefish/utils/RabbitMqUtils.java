package info.freefish.utils;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created By Freefish on 2021/6/3 0003
 */
public class RabbitMqUtils {
    private static  ConnectionFactory connectionFactory ;
    static {
        connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("h.freefish.info");
        connectionFactory.setPort(5672);
        connectionFactory.setVirtualHost("/test");//指定虚拟主机
        connectionFactory.setUsername("testuser");//指定用户
        connectionFactory.setPassword("123456");//指定密码
    }

    //定义提供连接的方法
    public static Connection getConnection() {
        //获取连接对象
        Connection connection = null;
        try {
            connection = connectionFactory.newConnection();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public static void close(Channel channel, Connection connection) {
        try {
            if (channel != null)
                channel.close();
            if (connection != null)
                connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
