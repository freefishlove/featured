package info.freefish.search;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonProperty;
import info.freefish.search.config.EsConfig;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.Avg;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created By Freefish on 2021/9/10 0010
 */
@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class SearchApplicationTests {


    @Autowired
    private RestHighLevelClient restHighLevelClient;

    @Test
    public void test() throws IOException {
        IndexRequest request = new IndexRequest("users");
        request.id("1");
        User user = new User("xu wei", 18, "F");
        String s = JSON.toJSONString(user);
        request.source(s, XContentType.JSON);
        IndexResponse index = restHighLevelClient.index(request, EsConfig.COMMON_OPTIONS);
        System.out.println(index);

    }

    @Test
    public void SearchData() throws IOException {
        SearchRequest search = new SearchRequest("bank");

        //知道DSL
        /**
         * GET bank/_search
         * {
         *   "query": {
         *     "match": {
         *       "address": "mill"
         *     }
         *   },
         *   "aggs": {
         *     "ageAgg": {
         *       "terms": {
         *         "field": "age",
         *         "size": 10
         *       },
         *       "aggs": {
         *         "avgAgg": {
         *           "avg": {
         *             "field": "age"
         *           }
         *         },
         *         "balanceAgg": {
         *           "avg": {
         *             "field": "balance"
         *           }
         *         }
         *       }
         *     }
         *   }
         * }
         */
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        SearchSourceBuilder query = searchSourceBuilder.query(QueryBuilders.matchQuery("address", "mill"));

        //年龄分布聚合
        TermsAggregationBuilder ageAgg = AggregationBuilders.terms("ageAgg").field("age").size(5);

        //年龄分布子聚合 avgAgg
        ageAgg.subAggregation(AggregationBuilders.avg("avgAgg").field("age"));
        //年龄分布子聚合 balanceAgg
        ageAgg.subAggregation(AggregationBuilders.avg("balanceAgg").field("balance"));


        query.aggregation(ageAgg);
        search.source(searchSourceBuilder);

        log.info("检索条件{}", query);

        SearchResponse ret = restHighLevelClient.search(search, RequestOptions.DEFAULT);
        log.info("检索结果{}", ret);
        SearchHit[] hits = ret.getHits().getHits();
        for (int i = 0; i < hits.length; i++) {
            System.out.println(JSON.parseObject(hits[i].getSourceAsString(), Acount.class));
        }

        //获取聚合信息
        Aggregations aggregations = ret.getAggregations();
        Terms ageAgg_ret = aggregations.get("ageAgg");

        List<? extends Terms.Bucket> buckets = ageAgg_ret.getBuckets();

        buckets.forEach(p -> {
            System.out.println("bucket:" + p.getKeyAsString());
            Aggregations aggs = p.getAggregations();
            Avg avgAgg = aggs.get("avgAgg");
            Avg balanceAgg = aggs.get("balanceAgg");
            System.out.println("avgAgg.getValue()" + avgAgg.getValue());
            System.out.println("balanceAgg.getValue()" + balanceAgg.getValue());
        });


    }

    public static void main(String[] args) {
        getNum(1, null);
    }

    public static void getNum(Integer a, Integer b) {
        System.out.println(a + b);
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    class User {
        private String name;
        private Integer age;
        private String gender;
    }

    @Data
    static class Acount {
        private Integer accountNumber;
        private Integer balance;
        private String firstname;
        private String lastname;
        private Integer age;
        private String gender;
        private String address;
        private String employer;
        private String email;
        private String city;
        private String state;
    }
}
