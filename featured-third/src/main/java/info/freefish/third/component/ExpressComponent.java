package info.freefish.third.component;


import info.freefish.third.utils.HttpUtils;
import lombok.Data;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created By Freefish on 2021/6/7 0007
 */
@Data
@Component
@ConfigurationProperties(prefix = "express")
public class ExpressComponent {
    private String host;
    private String path;
    private String appcode;

    public HttpEntity Express(String no, String type) {
        String method = "GET";
        Map<String, String> headers = new HashMap<String, String>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appcode);
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("no", no);
        querys.put("type", type);
        try {
            HttpResponse response = HttpUtils.doGet(host, path, method, headers, querys);
          //  HttpResponse response = HttpUtils.doGet("http://localhost:9501", "/sms/hello", method, headers, null);
            return response.getEntity();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
