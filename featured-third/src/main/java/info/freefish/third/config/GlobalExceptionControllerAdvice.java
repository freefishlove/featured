package info.freefish.third.config;


import info.freefish.common.exception.BizCodeEnume;
import info.freefish.common.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.Nullable;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.io.IOException;
import java.util.HashMap;

/**
 * 统一异常处理
 */
@Slf4j
@RestControllerAdvice(basePackages = "info.freefish.third.controller")
public class GlobalExceptionControllerAdvice {


    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public R handleValidException(MethodArgumentNotValidException e) {
        BindingResult bindingResult = e.getBindingResult();

        HashMap<String, String> errMap = new HashMap<>();

        bindingResult.getFieldErrors().forEach(item -> {
            errMap.put(item.getField(), item.getDefaultMessage());
        });
        return R.error(BizCodeEnume.VAILD_EXCEPTION.getCode(), BizCodeEnume.VAILD_EXCEPTION.getMsg()).put("data", errMap);
    }


    @ExceptionHandler(value = Throwable.class)
    public R handleException(Throwable e) {
        return R.error(BizCodeEnume.UNKNOW_EXCEPTION.getCode(), BizCodeEnume.UNKNOW_EXCEPTION.getMsg()).put("data", e.getMessage());

    }

    @ExceptionHandler(value = IOException.class)
    public R handleIoException(Throwable e) {
        return R.error(BizCodeEnume.UNKNOW_EXCEPTION.getCode(), BizCodeEnume.EXPRESS_SEND_EXCEPTION.getMsg()).put("data", e.getMessage());

    }
}
