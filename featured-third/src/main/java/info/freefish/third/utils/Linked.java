package info.freefish.third.utils;

import cn.hutool.core.io.FileUtil;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;

import java.util.concurrent.TimeUnit;

/**
 * Created By Freefish on 2021/7/22 0022
 */
public class Linked {
    public static void main(String[] args) {


        StringBuffer stringBuffer = new StringBuffer();

        Linked linked = new Linked();

        linked.addByOrder(new Node(1, "hello"));
        linked.addByOrder(new Node(5, "world"));
        linked.addByOrder(new Node(3, "fuck"));
        linked.addByOrder(new Node(4, "fuck"));
        linked.addByOrder(new Node(7, "fuck"));
        linked.show();
//        System.out.println("======================================");
//        linked.delete(new Node<>(3, ""));
//        linked.show();
        System.out.println("===============reverse=======================");
        Node reverse = linked.reverse();
        linked.show();

        for (int i = 0; i < 1000; i++) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }

    public void add(Node node) {
        Node<Integer> temp = head;
        while (true) {
            if (temp.next == null)
                break;
            temp = temp.next;
        }
        temp.next = node;
    }

    public void delete(Node<Integer> node) {

        Node temp = head;
        boolean flag = false;

        while (true) {
            if (temp.next == null) {
                System.out.println("链表为空改尼玛");
                return;
            }
            if (temp.next.getValue() == node.getValue()) {
                flag = true;
                break;
            }
            temp = temp.next;
        }
        if (flag) {
            temp.next = temp.next.next;

        } else {
            System.out.println("无元素可删除");
        }
    }

    public Node reverse() {
        Node header = head;
        if (header.next == null || header.next.next == null) return null;
        Node cur = header.next;
        Node next = null; //当前节点的下一个节点
        Node reverseHeader = new Node(0, "");
        while (cur != null) {
            next = cur.next;
            cur.next = reverseHeader.next;
            reverseHeader.next = cur;
            cur = next;
        }
        header.next = reverseHeader.next;
        head.next = reverseHeader.next;
        return head;

    }

    public void update(Node<Integer> node) {
        if (head.next == null) {
            System.out.println("链表为空改尼玛");
            return;
        }

        Node temp = head.next;
        boolean flag = false;

        while (true) {
            if (temp == null) {
                break;
            }
            if (temp.getValue() == node.getValue()) {
                flag = true;
                break;
            }

            temp = temp.next;
        }
        if (flag) {
            temp.setMessage(node.getMessage());
        } else {
            System.out.println("无元素可修改");
        }

    }


    public void addByOrder(Node<Integer> node) {
        Node<Integer> temp = head;
        boolean flag = false;

        while (true) {
            if (temp.next == null)
                break;

            if (temp.next.getValue() > node.getValue()) {
                break;
            } else if (temp.next.getValue() == node.getValue()) {
                flag = true;
                break;
            }
            temp = temp.next;
        }
        if (flag) {
            System.out.println("添加失败 已经存在");
            return;
        }
        node.next = temp.next;
        temp.next = node;
    }

    public void show() {
        if (head.next == null) {
            System.out.println("链表为空");
            return;
        }
        Node temp = head.next;

        while (true) {
            if (temp == null) {
                break;
            }
            System.out.println(temp.toString());
            temp = temp.next;
        }
    }


    private Node<Integer> head;

    Linked() {
        head = new Node<>();
    }


    static class Node<T> {
        public T getValue() {
            return value;
        }

        public Node(T value, String message) {
            this.message = message;
            this.value = value;
        }

        public void setValue(T value) {
            this.value = value;
        }

        private String message;

        @Override
        public String toString() {
            return "Node{" +
                    "message='" + message + '\'' +
                    ", value=" + value +
                    '}';
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        private T value;
        public Node<T> next;

        Node() {

        }

        Node(T t) {
            this.value = t;
        }

    }
}
