package info.freefish.third.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created By Freefish on 2021/7/20 0020
 */
public class LFU<k, v> {


    public LFU(Integer capacity) {
        this.capacity = capacity;
        map = new HashMap<>();//查找
        doubleLinkedList = new DoubleLink<>();
    }

    private Integer capacity;
    Map<k, Node<k, v>> map;
    DoubleLink<k, v> doubleLinkedList;

    class Node<k, v> {
        k key;
        v value;
        Node<k, v> prev;
        Node<k, v> next;

        public Node() {
            this.prev = this.next = null;
        }

        public Node(k key, v value) {
            this.key = key;
            this.value = value;
            this.prev = this.next = null;
        }
    }

    class DoubleLink<k, v> {

        Node<k, v> head;
        Node<k, v> tail;

        public DoubleLink() {
            head = new Node<>();
            tail = new Node<>();
            head.next = tail;
            tail.prev = head;
        }


        public void addHeader(Node<k, v> node) {
            node.next = head.next;
            node.prev = head.next.prev;//node.prev = head;
            head.next.prev = node;
            head.next = node;
        }

        public void remove(Node<k, v> node) {
            node.next.prev = node.prev;
            node.prev.next = node.next;
            node.next = null;
            node.prev = null;
        }

        // 5.获得最后一个节点
        public Node getLast() {
            return tail.prev;
        }
    }

    private void refreshDoubleLink(Node<k, v> node) {
        doubleLinkedList.remove(node);//获取节点时候 把数据删除添加到节点头
        doubleLinkedList.addHeader(node);
    }


    public v get(k key) {
        if (!map.containsKey(key)) return null;
        Node<k, v> node = map.get(key);
        refreshDoubleLink(node);
        return node.value;
    }


    public void put(k key, v value) {
        if (map.containsKey(key)) {
            Node<k, v> node = map.get(key);
            node.value = value;
            map.put(key, node);
            refreshDoubleLink(node);
        } else {
            if (map.size() == capacity) {
                Node last = doubleLinkedList.getLast();
                map.remove(last.key);
                doubleLinkedList.remove(last);
            }
            Node<k, v> nodes = new Node<>(key, value);
            map.put(key, nodes);

            doubleLinkedList.addHeader(nodes);
        }
    }


}

