package info.freefish.product.dao;

import info.freefish.product.entity.SkuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.sql.ResultSet;

/**
 * sku图片
 * 
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-06-03 14:17:27
 */
@Mapper
public interface SkuImagesDao extends BaseMapper<SkuImagesEntity> {

}
