package info.freefish.product.feign;

/**
 * Created By Freefish on 2021/6/4 0004
 */

import info.freefish.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("featured-sms")
public interface SmsFeignService {
    @GetMapping("/sms/hello")
    String helloSms();
}
