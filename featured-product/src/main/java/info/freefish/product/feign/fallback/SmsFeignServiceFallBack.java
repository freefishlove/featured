package info.freefish.product.feign.fallback;

import com.alibaba.fastjson.JSON;
import info.freefish.common.exception.BizCodeEnume;
import info.freefish.common.utils.R;
import info.freefish.product.feign.SmsFeignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Created By Freefish on 2021/6/4 0004
 */

@Slf4j
@Component
public class SmsFeignServiceFallBack implements SmsFeignService {

    @Override
    public String helloSms() {
        log.error("helloSms熔断机制触发");
        return JSON.toJSONString(R.error(BizCodeEnume.PRODUCT_FEIGN_EXCEPTION.getCode(), BizCodeEnume.PRODUCT_FEIGN_EXCEPTION.getMsg()));
    }
}
