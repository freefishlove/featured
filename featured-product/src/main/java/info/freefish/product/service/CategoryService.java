package info.freefish.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import info.freefish.common.utils.PageUtils;
import info.freefish.product.entity.CategoryEntity;
import info.freefish.product.vo.CategoryVo;

import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-06-03 14:17:27
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<CategoryEntity> listWithTree();

    List<CategoryVo> wxListWithTree();
}

