package info.freefish.product.service.impl;

import info.freefish.product.vo.CategoryVo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import info.freefish.common.utils.PageUtils;
import info.freefish.common.utils.Query;

import info.freefish.product.dao.CategoryDao;
import info.freefish.product.entity.CategoryEntity;
import info.freefish.product.service.CategoryService;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listWithTree() {
        //查出所有分类组装成父子结构
        List<CategoryEntity> entities = baseMapper.selectList(null);
        //找出所有一级分类
        List<CategoryEntity> collect = entities.stream().filter((CategoryEntity) -> CategoryEntity.getParentCid() == 0)
                .map((menu) -> {
                    menu.setChildren(getChildren(menu, entities));
                    return menu;
                }).sorted(Comparator.comparingInt(menu -> (menu.getSort() == null ? 0 : menu.getSort())))
                .collect(Collectors.toList());
        return collect;
    }

    @Override
    public List<CategoryVo> wxListWithTree() {
        //查出所有分类组装成父子结构
        List<CategoryEntity> entities = baseMapper.selectList(null);
        List<CategoryVo> collect = entities.stream().map(p -> {
            CategoryVo categoryVo = new CategoryVo();
            if (p.getCatLevel() == 1) {
                categoryVo.setId(p.getCatId());
                categoryVo.setName(p.getName());
            } else if (p.getCatLevel() == 2) {
                categoryVo.setId(p.getCatId());
                categoryVo.setName(p.getName());
                categoryVo.setPid(p.getParentCid());
            } else {
                categoryVo.setId(p.getCatId());
                categoryVo.setName(p.getName());
                categoryVo.setPid(p.getParentCid());
                categoryVo.setPicture(p.getIcon());
            }
            return categoryVo;
        }).collect(Collectors.toList());
        return collect;
    }


    private List<CategoryEntity> getChildren(CategoryEntity root, List<CategoryEntity> all) {
        List<CategoryEntity> collect = all.stream().filter(categoryEntity ->
                categoryEntity.getParentCid() == root.getCatId()).map(categoryEntity -> {
            categoryEntity.setChildren(getChildren(categoryEntity, all));
            return categoryEntity;
        }).sorted(Comparator.comparingInt(menu -> (menu.getSort() == null ? 0 : menu.getSort()))).collect(Collectors.toList());
        return collect;
    }
}