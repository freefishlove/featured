package info.freefish.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import info.freefish.common.utils.PageUtils;
import info.freefish.product.entity.BrandEntity;

import java.util.Map;

/**
 * 品牌
 *
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-06-03 14:17:27
 */
public interface BrandService extends IService<BrandEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

