package info.freefish.product.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class CategoryVo implements Serializable {
    private Long id;
    private Long pid;
    private String name;
    private String picture;
}
