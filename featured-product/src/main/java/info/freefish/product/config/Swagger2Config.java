//package info.freefish.product.config;
//
//import com.google.common.base.Predicates;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import springfox.documentation.builders.ApiInfoBuilder;
//import springfox.documentation.builders.PathSelectors;
//import springfox.documentation.service.ApiInfo;
//import springfox.documentation.service.Contact;
//import springfox.documentation.spi.DocumentationType;
//import springfox.documentation.spring.web.plugins.Docket;
//import springfox.documentation.swagger2.annotations.EnableSwagger2;
//
//@Configuration
//@EnableSwagger2
//public class Swagger2Config {
//    @Bean
//    public Docket webApiConfig() {
//        Docket docket = new Docket(DocumentationType.SWAGGER_2).groupName("webApi").apiInfo(webApiInfo()).select().
//                paths(Predicates.and(PathSelectors.regex("/.*"))).build();
//        return docket;
//    }
//
////    @Bean
////    public Docket adminWebApiConfig() {
////        Docket docket = new Docket(DocumentationType.SWAGGER_2).groupName("adminWebApi").apiInfo(adminWebApiInfo()).select().
////                paths(Predicates.and(PathSelectors.regex("/.*"))).build();
////        return docket;
////    }
//
//    //api 文档参数
//    private ApiInfo webApiInfo() {
//        return new ApiInfoBuilder()
//                .title("网站的Api文档")
//                .description("描述网站的API接口")
//                .version("2.0")
//                .contact(new Contact("freefish", "www.baidu.com", "1099550751@qq.com")).build();
//    }
//
////    //api 文档参数
////    private ApiInfo adminWebApiInfo() {
////        return new ApiInfoBuilder()
////                .title("网站的后台的Api文档")
////                .description("描述网站的API接口")
////                .version("2.0")
////                .contact(new Contact("freefish", "www.baidu.com", "1099550751@qq.com")).build();
////    }
//}