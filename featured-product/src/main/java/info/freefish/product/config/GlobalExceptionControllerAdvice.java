package info.freefish.product.config;


import info.freefish.common.exception.BizCodeEnume;
import info.freefish.common.utils.R;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;

/**
 * 统一异常处理
 */
//@ResponseBody  合成RestControllerAdvice
//@ControllerAdvice(basePackages ="info.freefish.gulimall.product.controller" )
@Slf4j
@RestControllerAdvice(basePackages = "info.freefish.product.controller")
public class GlobalExceptionControllerAdvice {


    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public R handleValidException(MethodArgumentNotValidException e) {
      //  log.error("数据校验出现问题{},异常类型{}", e.getMessage(), e.getClass());
        BindingResult bindingResult = e.getBindingResult();

        HashMap<String, String> errMap = new HashMap<>();

        bindingResult.getFieldErrors().forEach(item -> {
            errMap.put(item.getField(), item.getDefaultMessage());
        });
        return R.error(BizCodeEnume.VAILD_EXCEPTION.getCode(), BizCodeEnume.VAILD_EXCEPTION.getMsg()).put("data", errMap);
    }


    @ExceptionHandler(value = Throwable.class)
    public R handleException(Throwable e) {
        //log.error("Throwable Exception", e.getMessage(), e.getClass());
        return R.error(BizCodeEnume.UNKNOW_EXCEPTION.getCode(), BizCodeEnume.UNKNOW_EXCEPTION.getMsg()).put("data", e.getMessage());

    }
}
