package info.freefish.product.controller;

import info.freefish.common.dto.UserDTO;
import info.freefish.common.utils.R;
import info.freefish.product.feign.SmsFeignService;
import info.freefish.product.holder.LoginUserHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created By Freefish on 2021/6/4 0004
 */
@Slf4j
@RestController
@RequestMapping("/product")
public class UserController {

    @Autowired
    private LoginUserHolder loginUserHolder;

    @Autowired
    private SmsFeignService smsFeignService;

    @GetMapping("/currentUser")
    public UserDTO currentUser() {
        return loginUserHolder.getCurrentUser();
    }

    @GetMapping("/logTest")
    public void logTest() {
        log.info("<=================info================>");
        log.error("<=================error================>");
        log.warn("<=================warn================>");
        log.debug("<=================debug================>");
        log.trace("<=================trace================>");
    }

    @GetMapping("/feignTest")
    public R feignTest() {
        String s = smsFeignService.helloSms();
        return R.ok().setData(s);
    }
}
