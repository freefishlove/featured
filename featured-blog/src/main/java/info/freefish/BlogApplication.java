package info.freefish;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * Created By Freefish on 2021/7/8 0008
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "info.freefish.blog.feign")
@EnableConfigurationProperties
public class BlogApplication {
    public static void main(String[] args) {
        SpringApplication.run(BlogApplication.class, args);
    }
}
