package info.freefish.blog.to;

import lombok.Data;
import lombok.ToString;

/**
 * Created By Freefish on 2021/7/13 0013
 */
@Data
@ToString
public class UserInfoTo {
    /**
     *          jsonObject.get("user_name");
     *             jsonObject.get("id");
     *             jsonObject.get("exp");
     *             jsonObject.get("jti");
     *             jsonObject.get("client_id");
     */
    /**
     * 存储已登录用户在数据库中的ID
     */
    private Long userId;

    /**
     * 存储用户名
     */
    private String userName;
    /**
     * 过期时间
     */
    private Long exp;
    /**
     * jti
     */
    private String jti;
    /**
     * client_id
     */
    private String client_id;

    /**
     * 头像
     */
    private String avatar;


    /**
     * 介绍
     */
    private String introduction;
}
