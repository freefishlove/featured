package info.freefish.blog.dao;

import info.freefish.blog.entity.DetailsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-07-09 11:09:33
 */
@Mapper
public interface DetailsDao extends BaseMapper<DetailsEntity> {
	
}
