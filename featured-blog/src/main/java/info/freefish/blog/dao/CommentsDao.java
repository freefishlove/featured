package info.freefish.blog.dao;

import info.freefish.blog.entity.CommentsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-07-14 11:31:41
 */
@Mapper
public interface CommentsDao extends BaseMapper<CommentsEntity> {
	
}
