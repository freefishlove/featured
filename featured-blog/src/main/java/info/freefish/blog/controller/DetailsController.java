package info.freefish.blog.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ExecutionException;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.alibaba.csp.sentinel.SphU;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import info.freefish.blog.feign.AuthFeignService;
import info.freefish.blog.sentinel.CustomerBlockHandler;
import info.freefish.blog.sentinel.CustomerFallbackHandler;
import info.freefish.blog.vo.BlogDetailsVo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import info.freefish.blog.entity.DetailsEntity;
import info.freefish.blog.service.DetailsService;
import info.freefish.common.utils.PageUtils;
import info.freefish.common.utils.R;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


/**
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-07-09 11:09:33
 */
@Slf4j
@RestController
@RequestMapping("blog/details")
public class DetailsController {

    @Autowired
    AuthFeignService authFeignService;

    @Autowired
    private DetailsService detailsService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @SentinelResource(value = "list", fallbackClass = CustomerFallbackHandler.class, fallback = "handlerFallbackException",
            blockHandlerClass = CustomerBlockHandler.class, blockHandler = "handlerException")
    public R list(@RequestParam Map<String, Object> params) {

        R info = authFeignService.info(31L);
        log.info(String.valueOf(info));
        PageUtils page = detailsService.queryPage(params);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @SentinelResource(value = "info", blockHandlerClass = CustomerBlockHandler.class, blockHandler = "info",
            fallbackClass = CustomerFallbackHandler.class, fallback = "handlerInfoFallbackException")
    public R info(@PathVariable("id") Integer id) throws ExecutionException, InterruptedException {
        BlogDetailsVo details = detailsService.getDetailsBy(id);
        int count = 0;
        if (details.getComments() != null) {
            count = details.getComments().size();
        }
        if (id == 4) {
            throw new IllegalArgumentException("参数不可为4");
        }

        return R.ok().put("details", details).put("count", count);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("blog:details:save")
    public R save(@RequestBody DetailsEntity details) {
        detailsService.save(details);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("blog:details:update")
    public R update(@RequestBody DetailsEntity details) {
        detailsService.updateById(details);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("blog:details:delete")
    public R delete(@RequestBody Integer[] ids) {
        detailsService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
