package info.freefish.blog.sentinel;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeException;
import com.alibaba.csp.sentinel.slots.block.flow.FlowException;
import info.freefish.common.utils.R;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * Created By Freefish on 2021/9/1 0001
 */
public class CustomerBlockHandler {
    //保证参数与请求参数一致
    public static R handlerException(@RequestParam Map<String, Object> params, BlockException exception) {
        if (exception instanceof FlowException) {
            return R.error().setData("流控规则未通过。。");
        }
        if (exception instanceof DegradeException) {
            return R.error().setData("降级规则被创建。。");
        }
        return R.error().setData("业务繁忙请稍后后再试");
    }

    public R info(@PathVariable("id") Integer id) throws ExecutionException, InterruptedException {
        return R.error().setData("业务繁忙请稍后后再试");
    }
}
