package info.freefish.blog.sentinel;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import info.freefish.common.utils.R;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * Created By Freefish on 2021/9/1 0001
 */
public class CustomerFallbackHandler {
    //保证参数与请求参数一致
    public static R handlerFallbackException(Map<String, Object> params, Throwable e) {
        return R.error().setData("list=============handlerFallbackException");
    }

    public static R handlerInfoFallbackException(Integer id, Throwable e) {
        return R.error().setData("Info============handlerFallbackException");
    }

}
