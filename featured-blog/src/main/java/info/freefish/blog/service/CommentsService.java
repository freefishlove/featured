package info.freefish.blog.service;

import com.baomidou.mybatisplus.extension.service.IService;
import info.freefish.common.utils.PageUtils;
import info.freefish.blog.entity.CommentsEntity;

import java.util.Map;

/**
 * 
 *
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-07-14 11:31:41
 */
public interface CommentsService extends IService<CommentsEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

