package info.freefish.blog.service;

import com.baomidou.mybatisplus.extension.service.IService;
import info.freefish.blog.vo.BlogDetailsVo;
import info.freefish.common.utils.PageUtils;
import info.freefish.blog.entity.DetailsEntity;

import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-07-09 11:09:33
 */
public interface DetailsService extends IService<DetailsEntity> {

    PageUtils queryPage(Map<String, Object> params) ;

    BlogDetailsVo getDetailsBy(Integer id) throws ExecutionException, InterruptedException;

    static int getNum() {
        return 255;
    }

    default int getDefault() {
        return 255;
    }
}

