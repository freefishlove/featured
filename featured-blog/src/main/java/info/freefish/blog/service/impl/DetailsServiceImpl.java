package info.freefish.blog.service.impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.system.UserInfo;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import info.freefish.blog.entity.CommentsEntity;
import info.freefish.blog.feign.AuthFeignService;
import info.freefish.blog.service.CommentsService;
import info.freefish.blog.vo.BlogDetailsVo;
import info.freefish.blog.vo.CommentsVo;
import info.freefish.blog.vo.UserEntity;
import info.freefish.blog.vo.UserInfoVo;
import info.freefish.common.utils.R;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import info.freefish.common.utils.PageUtils;
import info.freefish.common.utils.Query;

import info.freefish.blog.dao.DetailsDao;
import info.freefish.blog.entity.DetailsEntity;
import info.freefish.blog.service.DetailsService;
import org.springframework.transaction.annotation.Transactional;


@Service("detailsService")
public class DetailsServiceImpl extends ServiceImpl<DetailsDao, DetailsEntity> implements DetailsService {

    @Autowired
    private AuthFeignService authFeignService;
    @Autowired
    private CommentsService commentsService;

    @Autowired
    ThreadPoolExecutor executor;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<DetailsEntity> page = this.page(
                new Query<DetailsEntity>().getPage(params),
                new QueryWrapper<DetailsEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    @Transactional //设计Feign  分布式事务的必要性 吃醋可以获取不到
    public BlogDetailsVo getDetailsBy(Integer id) throws ExecutionException, InterruptedException {

        BlogDetailsVo blogDetailsVo = new BlogDetailsVo();

        CompletableFuture<DetailsEntity> detailInfoFuture = CompletableFuture.supplyAsync(() -> {
            //1. 获取blog详细信息
            DetailsEntity detailsEntity = this.getById(id);
            blogDetailsVo.setBlogInfo(detailsEntity);
            return detailsEntity;
        }, executor);

        CompletableFuture<Void> userFuture = detailInfoFuture.thenAcceptAsync(detailsEntity -> {
            //2. 获取User 用户详细信息
            R info = authFeignService.info(detailsEntity.getAuthId().longValue());
            UserEntity userEntity = info.getData("user", new TypeReference<UserEntity>() {
            });
            UserInfoVo userInfoVo = new UserInfoVo();
            if (userEntity != null) {
                userInfoVo.setUserId(userEntity.getId());
                userInfoVo.setAvatar(userEntity.getAvatar());
                userInfoVo.setIntroduction(userEntity.getIntroduction());
                userInfoVo.setUserName(userEntity.getUsername());
            }
            blogDetailsVo.setUserInfo(userInfoVo);
        });
        CompletableFuture<Void> commentsFuture = CompletableFuture.runAsync(() -> {
            //3.获取评论信息  通过博客Id查询
            blogDetailsVo.setComments(getComments(id));
        }, executor);
        //detailInfoFuture 可以省略 因为他被其他关联
        CompletableFuture<Void> allFuture = CompletableFuture.allOf(detailInfoFuture, userFuture, commentsFuture);
        allFuture.get();

        return blogDetailsVo;
    }


    private List<CommentsEntity> getComments(Integer id) {
        QueryWrapper<CommentsEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("details_id", id);
        //查询所有的此博客的评论信息
        List<CommentsEntity> commentsEntities = commentsService.list(queryWrapper);
        if (commentsEntities != null && commentsEntities.size() > 0) {
            //查出一级评论
            List<CommentsEntity> collect = commentsEntities.stream().filter(p -> p.getParentId() == 0).collect(Collectors.toList());
            for (CommentsEntity p : collect) {
                p.setComments(getChildren(p, commentsEntities));
            }
            return collect;
        }
        return null;
    }

    /**
     * root 我根节点
     *
     * @param root
     * @param commentsEntities
     * @return
     */
    private List<CommentsEntity> getChildren(CommentsEntity root, List<CommentsEntity> commentsEntities) {
        List<CommentsEntity> collect = commentsEntities.stream().filter(p -> p.getParentId() == root.getId()).map(p -> {
            p.setComments(getChildren(p, commentsEntities));
            return p;
        }).collect(Collectors.toList());
        return collect;
    }
}