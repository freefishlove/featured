package info.freefish.blog.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 
 * 
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-07-09 11:09:33
 */
@Data
@Accessors(chain = true)
@TableName("blog_details")
public class DetailsEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private String headerImage;
	/**
	 * 
	 */
	private String text;
	/**
	 * 
	 */
	private String descrption;
	/**
	 * 
	 */

	private String images;

	private Integer authId;

	private Integer likes;
	/**
	 * 
	 */
	private Date createTime;
	/**
	 * 
	 */
	private Date updateTime;

}
