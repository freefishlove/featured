package info.freefish.blog.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;

/**
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-07-14 11:31:41
 */
@Data
@TableName("blog_comments")
public class CommentsEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId
    private Integer id;
    /**
     *
     */
    private Integer detailsId;
    /**
     * 用户id
     */
    private Integer userId;


    private String userName;


    /**
     * 头像
     */
    private String avatar;
    /**
     *
     */
    private Integer parentId;
    /**
     * 点赞数
     */
    private Integer likes;
    /**
     * 0 审核未通过  1 审核通过
     */
    private Integer status;
    /**
     * 具体内容
     */
    private String content;
    /**
     *
     */
    private Date createTime;
    /**
     *
     */
    private Date updateTime;

    @TableField(exist = false)
    private List<CommentsEntity> comments;

}
