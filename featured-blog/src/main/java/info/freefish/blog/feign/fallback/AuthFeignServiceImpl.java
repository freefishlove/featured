package info.freefish.blog.feign.fallback;

import info.freefish.blog.feign.AuthFeignService;
import info.freefish.common.exception.BizCodeEnume;
import info.freefish.common.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * Created By Freefish on 2021/7/13 0013
 * @author Administrator
 */
@Component
@Slf4j
public class AuthFeignServiceImpl implements AuthFeignService {
    @Override
    public R info(Long id) {
        log.error("AuthFeignServiceImpl熔断机制触发");
        return R.error(BizCodeEnume.TO_MANY_REQUEST.getCode(), BizCodeEnume.TO_MANY_REQUEST.getMsg());
    }
}
