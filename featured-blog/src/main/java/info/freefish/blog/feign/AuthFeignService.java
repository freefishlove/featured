package info.freefish.blog.feign;

import info.freefish.blog.feign.fallback.AuthFeignServiceImpl;
import info.freefish.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(value = "featured-auth",fallback = AuthFeignServiceImpl.class)
public interface AuthFeignService {
    @RequestMapping("core/user/info/{id}")
    R info(@PathVariable("id") Long id);
}
