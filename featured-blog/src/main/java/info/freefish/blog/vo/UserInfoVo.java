package info.freefish.blog.vo;

import lombok.Data;
import lombok.ToString;

/**
 * Created By Freefish on 2021/7/13 0013
 */
@Data
@ToString
public class UserInfoVo {
    /**
     *          jsonObject.get("user_name");
     *             jsonObject.get("id");
     *             jsonObject.get("exp");
     *             jsonObject.get("jti");
     *             jsonObject.get("client_id");
     */
    /**
     * 存储已登录用户在数据库中的ID
     */
    private Long userId;
    /**
     * 存储用户名
     */
    private String userName;
    /**
     * 头像
     */
    private String avatar;


    /**
     * 介绍
     */
    private String introduction;
}
