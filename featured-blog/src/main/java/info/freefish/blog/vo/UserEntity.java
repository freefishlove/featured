package info.freefish.blog.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户信息表
 * 
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-05-24 15:20:25
 */
@Data
public class UserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	private Long id;
	/**
	 * 用户名
	 */
	private String username;
	/**
	 * 昵称
	 */
	private String nickname;
	/**
	 * 性别
	 */
	private Integer gender;
	/**
	 * 密码
	 */
	private String password;
	/**
	 * 部门ID
	 */
	private Integer deptId;
	/**
	 * 删除标识（0未删除 1已删除）
	 */
	private Integer deleted;
	/**
	 * 用户头像
	 */
	private String avatar;
	/**
	 * 联系方式
	 */
	private String mobile;
	/**
	 * 用户状态（0正常 1禁用）
	 */
	private Integer status;
	/**
	 * 用户邮箱
	 */
	private String email;

	private String introduction;

	/**
	 * 创建时间
	 */
	private Date gmtCreate;
	/**
	 * 更新时间
	 */
	private Date gmtModified;

}
