package info.freefish.blog.vo;

import info.freefish.blog.entity.CommentsEntity;
import info.freefish.blog.entity.DetailsEntity;
import info.freefish.blog.to.UserInfoTo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * Created By Freefish on 2021/7/13 0013
 */
@Data
public class BlogDetailsVo implements Serializable {
    private DetailsEntity blogInfo;

    private UserInfoVo userInfo;

    private List<CommentsEntity> comments;

}
