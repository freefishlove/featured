package info.freefish.blog.config;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import lombok.experimental.Tolerate;
import lombok.var;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created By Freefish on 2021/7/12 0012
 */
@Component
public class HttpConfig implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        var user = request.getHeader("user");
        if (user != null && StrUtil.isEmpty(JSON.parseObject(user).toJSONString()))
            response.addHeader("user", JSON.parseObject(user).toJSONString());
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }
}
