package info.freefish.blog.Interceptor;

import cn.hutool.json.JSON;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import info.freefish.blog.to.UserInfoTo;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created By Freefish on 2021/7/13 0013
 */
public class BlogInterceptor implements HandlerInterceptor {
    public static ThreadLocal<UserInfoTo> threadLocalUser = new ThreadLocal<>();

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        String user = request.getHeader("user");
        if (user != null) {
            UserInfoTo userInfo = new UserInfoTo();
            JSONObject jsonObject = JSONUtil.parseObj(user);
            userInfo.setUserName(jsonObject.getStr("user_name"));
            userInfo.setUserId(jsonObject.getLong("id"));
            userInfo.setExp(jsonObject.getLong("exp"));
            userInfo.setJti(jsonObject.getStr("jti"));
            userInfo.setClient_id(jsonObject.getStr("client_id"));
            threadLocalUser.set(userInfo);
        }

        return HandlerInterceptor.super.preHandle(request, response, handler);
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}