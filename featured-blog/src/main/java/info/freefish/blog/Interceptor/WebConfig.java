package info.freefish.blog.Interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Created By Freefish on 2021/7/13 0013
 */
@Component
public class WebConfig implements WebMvcConfigurer, HandlerInterceptor {

    //配置拦截器的拦截路径

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new BlogInterceptor()).addPathPatterns("/**");
    }



}
