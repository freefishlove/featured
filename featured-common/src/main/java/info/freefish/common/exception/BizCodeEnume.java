package info.freefish.common.exception;
/***
 * 错误码和错误信息定义类
 * 1. 错误码定义规则为5为数字
 * 2. 前两位表示业务场景，最后三位表示错误码。例如：100001。10:通用 001:系统未知异常
 * 3. 维护错误码后需要维护错误描述，将他们定义为枚举形式
 * 错误码列表：
 *  10: 通用
 *      001：参数格式校验
 *  11: 商品
 *  12: 订单
 *  13: 购物车
 *  14: 物流
 *
 *
 */
public enum BizCodeEnume {
    UNKNOW_EXCEPTION(10000,"系统未知异常"),
    VAILD_EXCEPTION(10001,"参数格式校验失败"),
    VAILD_SMSCODE(10002,"短信验证码频率太高，稍后再试"),
    USER_EXIST_EXCEPTION(15001,"用户已经存在"),
    PHONE_EXIST_EXCEPTION(15002,"手机号码已经存在"),
    LOGINACCT_PASSWORD_INVALID(15003,"登录账户密码错误"),
    LOGIN_VAILED_EXCEPTION(15005,"认证授权失败"),
    NO_STOCK_EXCEPTION(21000,"没有库存"),
    PRODUCT_FEIGN_EXCEPTION(11007,"远程调用超时"),
    EXPRESS_SEND_EXCEPTION(11015,"查询快递接口异常"),
    USER_EXIT_OR_OTHER_EXCEPTION(11016,"用户注册失败"),
    AUTH_SERVER_FAILED(11017,"授权出错"),
    TO_MANY_REQUEST(11018,"请求过多"),

    PRODUCT_UP_EXCEPTION(11000,"商品上架失败");


    private int code;
    private String msg;
    BizCodeEnume(int code,String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
