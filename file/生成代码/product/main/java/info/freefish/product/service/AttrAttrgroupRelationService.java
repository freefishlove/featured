package info.freefish.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import info.freefish.common.utils.PageUtils;
import info.freefish.product.entity.AttrAttrgroupRelationEntity;

import java.util.Map;

/**
 * 属性&属性分组关联
 *
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-06-03 14:17:27
 */
public interface AttrAttrgroupRelationService extends IService<AttrAttrgroupRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

