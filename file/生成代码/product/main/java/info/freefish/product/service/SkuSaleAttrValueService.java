package info.freefish.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import info.freefish.common.utils.PageUtils;
import info.freefish.product.entity.SkuSaleAttrValueEntity;

import java.util.Map;

/**
 * sku销售属性&值
 *
 * @author freefish
 * @email 1099550751@qq.com
 * @date 2021-06-03 14:17:27
 */
public interface SkuSaleAttrValueService extends IService<SkuSaleAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

